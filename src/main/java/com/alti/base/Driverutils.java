package com.alti.base;

import com.alti.common.ConfigReader;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.log4testng.Logger;
import sun.security.krb5.Config;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Driverutils {
	static Logger log = Logger.getLogger(Driverutils.class);
	public static WebDriver driver;
	
	public static WebDriver getDriver() {
		return driver;
	}

	public static void launchApplication(String url,String browserType) throws Exception{
		initWebDriver(url,browserType);

	}
	
	// Find the best solution to launch mobileapp without passing a dummy input variable "url"
	
	public static void tearDown() {
		driver.quit();
	}
	
	
	
	public static String getURL() {
		if(ConfigReader.getProperty("weburl")==null){
			log.info("URL is undefined Stopping Test Execution");
			driver.quit();
				
		}
		return ConfigReader.getProperty("weburl");
	}
	
	private static WebDriver initWebDriver(String url,String browser) throws Exception {
		switch(browser) {

			case "chrome" :
			String chromeNodeUrl = ConfigReader.getProperty("chromeNodeUrl");
			DesiredCapabilities capability = DesiredCapabilities.chrome();
			capability.setBrowserName("chrome");
			driver = new RemoteWebDriver(new URL(chromeNodeUrl),capability);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
			driver.navigate().to(getURL());
			break;

			case "firefox" :
			String firefoxNodeUrl = ConfigReader.getProperty("firefoxNodeUrl");
			DesiredCapabilities capability2 = DesiredCapabilities.firefox();
			capability2.setBrowserName("firefox");
			driver = new RemoteWebDriver(new URL(firefoxNodeUrl),capability2);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
			driver.navigate().to(getURL());
			break;

		case "mobapp" :
			DesiredCapabilities cap= new DesiredCapabilities();
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, ConfigReader.getProperty("platform_name"));
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, ConfigReader.getProperty("device_name"));
			cap.setCapability(MobileCapabilityType.UDID, ConfigReader.getProperty("udid"));
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, ConfigReader.getProperty("platform_version"));
			cap.setCapability(MobileCapabilityType.APP,"applocationgoeshere");
			cap.setCapability(MobileCapabilityType.FULL_RESET,"true");
			driver= new AndroidDriver<WebElement>(new URL("http://34.93.216.187:4723/wd/hub"),cap);
			break;
			
			
		}
		return driver;
	}

}
