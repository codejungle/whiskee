package com.alti.reporter;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {
	static ExtentReports extent;
	final static String filepath = "./test-output/html/Extent.html";
	
	public synchronized static ExtentReports getReporter() {
		if(extent == null) {
			extent = new ExtentReports(filepath,true);
		}
		return extent;
	}

}
