package com.alti.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import static io.restassured.RestAssured.given;
import com.alti.constants.Paths;
import com.alti.reporter.ExtentManager;
/*import com.alti.dataprovider.ExcelDataProvider;
import com.alti.reporter.ExtentManager;
import com.alti.reporter.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;*/
import com.alti.reporter.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class Specifications {
	
	Logger log= Logger.getLogger(Specifications.class);
    public RequestSpecification reqSpec;
    public  ResponseSpecification resSpec;
    static Map<String, Object> testcaseMap;
    protected static String accessToken = null;


   
   // @BeforeMethod
    public void beforeMethod(Method method) {
    	ExtentTestManager.startTest(method.getName());
    	
    }
    
    //@AfterMethod
    protected void afterMethod(ITestResult result) {
    	if(result.getStatus()== ITestResult.FAILURE) {
    		ExtentTestManager.getTest().log(LogStatus.FAIL, result.getThrowable());
    	}else if(result.getStatus() == ITestResult.SKIP) {
    		ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped", result.getThrowable());
    	} else {
    		ExtentTestManager.getTest().log(LogStatus.PASS, "Test Passed");
    	}
    	ExtentManager.getReporter().endTest(ExtentTestManager.getTest());
    	ExtentManager.getReporter().flush();
    }
   
    protected String getStackTrace(Throwable t) {
    	StringWriter sw= new StringWriter();
    	PrintWriter pw= new PrintWriter(sw);
    	t.printStackTrace(pw);
    	return sw.toString();
    	
    }
   
}
