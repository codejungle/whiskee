#Author: rchandra@altimetrik.com

Feature: Different Operation on Favrorite API 
  

  
  Scenario Outline: Favoriting an Event
    Given make a request with post method with "<entityid>" and "<devicetype>" and "<source>" and "<hmacid>" 
		Then verify the response code should be "<responsecode>" 
		And verify the favouriteid 
		Then launch webApplication with url "<url>" and browser "<browser>"
	
	
	Examples:
	
		| entityid | devicetype | source | hmacid | responsecode | url | browser |
		| K8vZ9171Jo7  | DESKTOP | TMAPP  | cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7 | 200 | https://ticketmaster.com | firefox |
		| K8vZ9171Jo7  | DESKTOP | TMAPP  | cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7 | 200 | https://ticketmaster.com | chrome |
		| K8vZ9171Jo7  | DESKTOP | TMAPP  | cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7 | 200 | https://ticketmaster.com | mobapp |
		

  Scenario: list all the favorite from my favorite page 
		Given make an GET Api Request 
		Then List all the events