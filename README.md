# Whiskee

A test framework for :
- UI Tests with Selenium
- Mobile Tests with Appium
- API mocking with WireMock Library


How to get started?
Check out some tests in src/test/java/tests. 

**The framework supports Cucumber and TestNG runner.**

#Test Environments

To spin off docker containers, kindly use the `docker-compose.yml`:

- Selenium Grid @ port `4444` with FF and Chrome Browser attached to the Grid.
- Wiremock Server @ port `9000`